#!/usr/bin/env bash

ZABBIX_DIR=/app/zabbix

cd ${ZABBIX_DIR}/src/zabbix
./bootstrap.sh
./configure --prefix=${ZABBIX_DIR}/zabbix --enable-server --enable-agent --with-mysql --with-libcurl --with-libxml2 --with-libevent=${ZABBIX_DIR}/libevent
make dbschema
make install

# We want to have configs in the mount. But mounts are not available during the build.
# So configs are copied to the /app/tmp. And from there they will be copied to the mount during the build.
TEMP_STORAGE=${ZABBIX_DIR}/tmp
mkdir -p ${TEMP_STORAGE}
cp ${ZABBIX_DIR}/zabbix/etc/zabbix_server.conf ${TEMP_STORAGE}/zabbix_server.conf
cp ${ZABBIX_DIR}/zabbix/etc/zabbix_agentd.conf ${TEMP_STORAGE}/zabbix_agentd.conf
mkdir -p ${TEMP_STORAGE}/ui
cp ${ZABBIX_DIR}/src/zabbix/ui/conf/maintenance.inc.php ${TEMP_STORAGE}/ui/maintenance.inc.php
cp ${ZABBIX_DIR}/src/zabbix/ui/conf/zabbix.conf.php.example ${TEMP_STORAGE}/ui/zabbix.conf.php.example

# And the fake config links are created for the mount path.
# Everything we store inside the mount during the build will be overridden by the actual amount during the deploy.
MOUNT_PATH=${ZABBIX_DIR}/configs
mkdir -p ${MOUNT_PATH}
rm ${ZABBIX_DIR}/zabbix/etc/zabbix_server.conf
cp ${TEMP_STORAGE}/zabbix_server.conf ${MOUNT_PATH}/zabbix_server.conf
ln -s ${MOUNT_PATH}/zabbix_server.conf ${ZABBIX_DIR}/zabbix/etc/zabbix_server.conf
rm ${ZABBIX_DIR}/zabbix/etc/zabbix_agentd.conf
cp ${TEMP_STORAGE}/zabbix_agentd.conf ${MOUNT_PATH}/zabbix_agentd.conf
ln -s ${MOUNT_PATH}/zabbix_agentd.conf ${ZABBIX_DIR}/zabbix/etc/zabbix_agentd.conf

mkdir -p ${MOUNT_PATH}/ui
rm -rf ${ZABBIX_DIR}/src/zabbix/ui/conf
ln -s ${MOUNT_PATH}/ui ${ZABBIX_DIR}/src/zabbix/ui/conf
cp ${TEMP_STORAGE}/ui/* ${MOUNT_PATH}/ui

# Zabbix home
ZABBIX_HOME=/app
touch ${MOUNT_PATH}/.my.cnf
ln -s ${MOUNT_PATH}/.my.cnf ${ZABBIX_HOME}/.my.cnf