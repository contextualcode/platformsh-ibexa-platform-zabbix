#!/usr/bin/env bash

ZABBIX_DIR=/app/zabbix

# File which is created once Zabbix DB is installed
IS_DB_INSTALLED=${ZABBIX_DIR}/configs/db.installed

if [ ! -f "$IS_DB_INSTALLED" ]; then
    platform db:sql -r database_zabbix 'ALTER DATABASE zabbix CHARACTER SET utf8 COLLATE utf8_bin;'
    platform db:sql -r database_zabbix < ${ZABBIX_DIR}/src/zabbix/database/mysql/schema.sql
    platform db:sql -r database_zabbix < ${ZABBIX_DIR}/src/zabbix/database/mysql/images.sql
    platform db:sql -r database_zabbix < ${ZABBIX_DIR}/src/zabbix/database/mysql/data.sql

    touch $IS_DB_INSTALLED
fi