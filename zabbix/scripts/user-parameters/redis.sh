#!/bin/bash

function query() {
    platform service:redis-cli info -r status_redis -A app --quiet | grep $1":" | grep -o [\.0-9]*
}

if [ $# == 0 ]; then
    echo $"Usage $0 {used_memory|used_memory_dataset|used_memory_dataset_perc|maxmemory|instantaneous_ops_per_sec|keyspace_hits|keyspace_misses}"
    exit
else
    query "$1"
fi