#!/bin/bash

function query() {
    cachetool opcache:status --fcgi=$SOCKET | grep "$1" | awk -F '|' '{print $3}' | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//' |  awk '/ TiB$/{printf "%u\n", $1*(1024*1024*1024*1024);next};/ GiB$/{printf "%u\n", $1*(1024*1024*1024);next};/ MiB$/{printf "%u\n", $1*(1024*1024);next};/ KiB$/{printf "%u\n", $1*1024;next};/ b$/{print $1;next};/.*/{print $0;next}'
}

if [ $# == 0 ]; then
    echo $"Usage $0 {Enabled|Memory used|Memory free|Strings buffer size|Strings memory used|Strings memory free|Number of strings|Cached scripts|Cached keys|Max cached keys|Oom restarts|Hash restarts|Manual restarts|Hits|Misses|Opcache hit rate}"
    exit
else
    query "$1"
fi