#!/usr/bin/env bash

cd /app/zabbix/src/zabbix/ui
composer install --no-dev --prefer-dist --no-progress --no-interaction --optimize-autoloader